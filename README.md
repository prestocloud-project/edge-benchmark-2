# PrestoCloud Edge Benchmark v2

This directory contains the PrEstoCloud edge device benchmark.
This version does not use docker and sends automatically the results to the PrEstoCloud Broker.

Requirements
------------

Python 2.7, Python pip, sysbench


Installation
-------------
1) Install python 2.7 and the required libraries from the file requirements.txt
```
$ pip install -r requirements.txt 
```
2) Install sysbench
```
sudo apt install sysbench
```

Configuration
----------
Edit config.properties as in the following example :

```

[Broker]
#the IP address of PrEstoCloud Broker (RabbitMQ)
rmqhost=1.2.3.4 
#Broker username
rmquser=youruser   
#Broker password
rmqpass=yourpass   
#Broker exchange name
exchange=presto.cloud 


[Benchmark]
#will publish results on this topic
topic=edge.benchmark.results 
#the fragment ID in the event payload
fragid=edge-benchmark-v2	  
#sysbench event number
events=100

```

Execution
---------
run the python script

