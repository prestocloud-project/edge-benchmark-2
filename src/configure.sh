#!/bin/sh
BROKER_IP=$1
BROKER_USER=$2
BROKER_PASSWORD=$3
FRAGMENT_ID=$4
cat config.properties.tpl | sed s/@broker_ip/$BROKER_IP/g | sed s/@broker_username/$BROKER_USER/g | sed s/@broker_password/$BROKER_PASSWORD/ | sed s/@fragment_id/$FRAGMENT_ID/ | tee config.properties
