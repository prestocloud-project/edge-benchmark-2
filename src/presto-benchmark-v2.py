#!/usr/bin/python
import os
import sys
import platform
import ifaddr
import subprocess
import time
import pika
import ConfigParser
import requests
import datetime
import json


config = ConfigParser.RawConfigParser()
config.read('config.properties')

#Broker props
rmqhost  = config.get('Broker', 'rmqhost');
rmquser  = config.get('Broker', 'rmquser');
rmqpass  = config.get('Broker', 'rmqpass');
exchange = config.get('Broker', 'exchange');
#Benchmark props
topic         = config.get('Benchmark', 'topic');
fragmentid    = config.get('Benchmark', 'fragid');
events        = config.get('Benchmark', 'events');
threads       = config.get('Benchmark', 'threads');
MBytes        = config.get('Benchmark', 'MBytes');
mutex_loops   = config.get('Benchmark', 'mutex-loops');


def get_fragment_id():
  frag_id = None
  adapters = ifaddr.get_adapters()

  for adapter in adapters:
    print "Adapter :",adapter.name
    #if adapter.name != "lo":
    if "tun0" in adapter.name :
      for ip in adapter.ips:
        #print type(ip.ip)
        if type(ip.ip) is tuple:
          print "\tFound V6 IP :", ip.ip
          frag_id = ip.ip[0]
          if (not frag_id.startswith("fe80")) and (not frag_id.startswith("fd")):
             print "\t\t", frag_id , " is public. Assigned to deviceID !"
             return frag_id
          print frag_id
  return frag_id

def executeBenchmarks(events):
  dt = 0
  start = time.time()
  subprocess.call(["sysbench","--test=memory", "run","--memory-total-size="+str(MBytes)+"M","--max-time=0"])
  subprocess.call(["sysbench","--test=cpu",    "run","--max-requests="+str(events),"--max-time=0"])
  subprocess.call(["sysbench","--test=threads","run","--num-threads="+str(threads),"--max-time=0"])
  subprocess.call(["sysbench","--test=mutex",  "run","--mutex-loops="+str(mutex_loops),"--max-time=0"])
  end = time.time()
  dt = (end-start)
  return dt

def publishToPresto(channel,deviceID,bTime):
  _payload =  {}
  _payload['fragid']   = str(fragmentid).strip()
  _payload['res_inst'] = str(deviceID).strip()
  _payload['task_exec_time_ms'] = int ( bTime * 1000 )
  payload = json.dumps(_payload,sort_keys=True)
  channel.basic_publish(
       exchange=exchange, routing_key=topic,
       body=payload)
  print "PUBLISHED BENCHMARK RESULTS"
  print json.dumps(_payload,indent=4, sort_keys=True)

def connect():
  credentials = pika.PlainCredentials(rmquser ,rmqpass)
  connection  = pika.BlockingConnection(
        pika.ConnectionParameters(host=rmqhost,credentials=credentials)
        )

  channel = connection.channel()
  channel.exchange_declare(
        exchange=exchange,
        exchange_type='topic',
        durable=True
        )
  result = channel.queue_declare('',exclusive=True)
  queue_name = result.method.queue
  print "queue_name :",queue_name
  channel.queue_bind (
        exchange=exchange,
        queue=queue_name,
        routing_key=topic
        )
  return channel


def main():
  print(platform.uname()) 
  print "="*80
  deviceID = get_fragment_id()
  print "Device ID:", deviceID
  bTime = executeBenchmarks(events)
  print "Benchmark time:",bTime
  channel = connect()
  publishToPresto(channel,deviceID,bTime)
  print "="*80


if __name__ == "__main__":
    main()
