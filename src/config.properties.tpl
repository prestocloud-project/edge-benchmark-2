[Broker]
rmqhost=@broker_ip
rmquser=@broker_username
rmqpass=@broker_password
exchange=presto.cloud

[Benchmark]
#will publish results on this topic
topic=edge.benchmark.results 
#the fragment ID in the event payload
fragid=@fragment_id       
#sysbench 
events=1000
MBytes=10000
threads=1000
mutex-loops=10000
